package yaml

import (
	"gopkg.in/yaml.v2"
	"io/ioutil"
	"log"
	"os"
)

var (
	hcw Hcw
)

func init() {
	readCtgYaml()
}

type Cfg struct {
	Hcw Hcw `yaml:"hcw"`
}

type Hcw struct {
	Mysql  Mysql
	Redis  Redis
	Server string
	Eureka Eureka
}

type Mysql struct {
	Username string
	Password string
	Host     string
	Port     string
	Db       string
	Timezone string
}

type Redis struct {
	Addr     string
	Port     string
	Password string
	Db       int
}

type Eureka struct {
	Username     string
	Password     string
	RegisterAddr string
	Application  string
	Port         string
	Enable       bool
}

func readCtgYaml() {
	file, err := os.Open("./config/cfg.yaml")
	if err == nil {
		bytes, err := ioutil.ReadAll(file)
		if err != nil {
			panic(err)
		}
		cfg := Cfg{}
		err = yaml.Unmarshal(bytes, &cfg)
		if err != nil {
			panic(err)
		}
		hcw = cfg.Hcw
		log.Print("[fiberHcw] [yaml]: ", hcw)
	}
}

func GetRedisCtg() Redis {
	return hcw.Redis
}

func GetMysqlCtg() Mysql {
	return hcw.Mysql
}

func GetServerCtg() string {
	return hcw.Server
}

func GetEurekaCtg() Eureka {
	return hcw.Eureka
}
