package httpRestTemplate

import (
	"bytes"
	"encoding/json"
	"net/http"
)

func Send(url string, requestBody any, httpHeaders map[string]string, method string) *http.Response {
	var req *http.Request
	if requestBody != nil {
		b, _ := json.Marshal(requestBody)
		req, _ = http.NewRequest(method, url, bytes.NewReader(b))
	} else {
		req, _ = http.NewRequest(method, url, nil)
	}
	if httpHeaders != nil {
		for k, v := range httpHeaders {
			req.Header.Set(k, v)
		}
	}
	httpClient := &http.Client{}
	response, _ := httpClient.Do(req)
	defer response.Body.Close()
	return response
}
