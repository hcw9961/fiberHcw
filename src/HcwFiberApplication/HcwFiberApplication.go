package HcwFiberApplication

import (
	"gitee.com/hcw9961/fiberHcw/src/config"
	"gitee.com/hcw9961/fiberHcw/src/interceptorFilter"
	"gitee.com/hcw9961/fiberHcw/src/request"
	"github.com/gofiber/fiber/v2"
	"log"
	"sort"
)

var app *fiber.App

func init() {
	app = fiber.New()
	app.Get("/hello/hcw", func(c *fiber.Ctx) error {
		return c.SendString("Hello, HCW!")
	})
}

func Start() {
	setInterceptor()
	for _, m := range request.Mappings {
		if m.Method == "GET" {
			app.Get(m.Path, m.Handler)
			log.Print("[fiberHcw] [API][BUILD][GET]: ", m)
		} else if m.Method == "POST" {
			app.Post(m.Path, m.Handler)
			log.Print("[fiberHcw] [API][BUILD][POST]: ", m)
		} else if m.Method == "PUT" {
			app.Put(m.Path, m.Handler)
			log.Print("[fiberHcw] [API][BUILD][PUT]: ", m)
		} else if m.Method == "DELETE" {
			app.Delete(m.Path, m.Handler)
			log.Print("[fiberHcw] [API][BUILD][DELETE]: ", m)
		} else {
			log.Print("[fiberHcw] [API][BUILD][UNKNOWN]: ", m)
		}
	}
	if config.GetServer() == "" {
		app.Listen(":8080")
	} else {
		app.Listen(":" + config.GetServer())
	}
}

func setInterceptor() {
	log.Print("[fiberHcw] [interceptorFilter]: start set interceptors...")
	is := interceptorFilter.GetInterceptorCollector()
	log.Print("[fiberHcw] [interceptorFilter]: GetInterceptorCollector: ", is)
	if len(is) != 0 {
		sort.Sort(sort.Reverse(is))
		for _, i := range is {
			patterns := interceptorFilter.GetInterceptorUrlRegexPattern(i)
			for _, p := range patterns {
				app.Use(p, interceptorFilter.GetInterceptorPreHandler(i))
				log.Print("[fiberHcw] [interceptorFilter]: add use interceptorFilter: "+interceptorFilter.GetName(i), " pattern is: "+p)
			}
		}
	}
}
