package config

import (
	"gitee.com/hcw9961/fiberHcw/src/yaml"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"log"
	"strings"
)

var (
	db       *gorm.DB
	errDb    error
	mysqlCtg yaml.Mysql
)

func init() {
	mysqlCtg = yaml.GetMysqlCtg()
	if mysqlCtg.Username != "" {
		initMysql(mysqlCtg.Username, mysqlCtg.Password, mysqlCtg.Host, mysqlCtg.Port, mysqlCtg.Db, mysqlCtg.Timezone)
	}
}

func initMysql(username string, password string, host string, port string, dbName string, timezone string) {
	timezoneReplace := strings.Replace(timezone, "/", "%2F", -1)
	if timezone == "" {
		timezoneReplace = "Asia%2FShanghai"
	}
	// 初始化数据库链接
	dns := username + ":" + password + "@tcp" + "(" + host + ":" + port + ")" + "/" +
		dbName + "?charset=utf8mb4&parseTime=True&loc=" + timezoneReplace
	db, errDb = gorm.Open(mysql.Open(dns), &gorm.Config{})
	if errDb != nil {
		log.Println("[fiberHcw] [mysql]: mysql content faild, err: ", errDb)
		return
	}
	log.Println("[fiberHcw] [mysql]: mysql connect success")
}

func getMysqlClient() *gorm.DB {
	return db
}
