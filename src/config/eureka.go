package config

import (
	e "gitee.com/hcw9961/fiberHcw/src/eureka"
	"gitee.com/hcw9961/fiberHcw/src/yaml"
	"log"
)

var eurekaCtg yaml.Eureka

func init() {
	eurekaCtg = yaml.GetEurekaCtg()
	if eurekaCtg.Enable {
		initEureka(eurekaCtg)
		log.Println("[fiberHcw] [eureka]: eureka client success::" +
			" registerAddr:" + eurekaCtg.RegisterAddr +
			" port:" + eurekaCtg.Port +
			" application:" + eurekaCtg.Application)
	}
}

func initEureka(eureka yaml.Eureka) {
	m2 := make(map[string]string)
	if len(eureka.Username) != 0 && len(eureka.Password) != 0 {
		m2["username"] = eureka.Username
		m2["password"] = eureka.Password
	}
	go e.RegisterClient(eureka.RegisterAddr, "", eureka.Application, eureka.Port, "43", m2)
}
