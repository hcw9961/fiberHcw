package config

import (
	"github.com/go-redis/redis"
	"gorm.io/gorm"
)

func GetMysqlClient() *gorm.DB {
	return getMysqlClient()
}

func GetRedisClient() *redis.Client {
	return getRedisClient()
}

func GetServer() string {
	return getServer()
}
