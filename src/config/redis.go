package config

import (
	"errors"
	"gitee.com/hcw9961/fiberHcw/src/yaml"
	"github.com/go-redis/redis"
	"log"
)

var rdb *redis.Client
var redisCtg yaml.Redis

func init() {
	redisCtg = yaml.GetRedisCtg()
	if redisCtg.Addr != "" {
		initRedis(redisCtg.Addr, redisCtg.Password, redisCtg.Db)
	}
}

func initRedis(addr string, password string, db int) {
	// 初始化缓存
	rdb = redis.NewClient(&redis.Options{
		Addr:     addr,
		Password: password,
		DB:       db,
	})
	// 通过 client.Ping() 来检查是否成功连接到了 redis 服务器
	_, err := rdb.Ping().Result()
	if err != nil {
		errors.New("[fiberHcw] [redis]: redis connect fail")
	}
	log.Println("[fiberHcw] [redis]: redis connect success")
}

func getRedisClient() *redis.Client {
	return rdb
}
