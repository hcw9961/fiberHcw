package config

import (
	"gitee.com/hcw9961/fiberHcw/src/yaml"
)

var server string

func init() {
	server = yaml.GetServerCtg()
}

func getServer() string {
	return server
}
