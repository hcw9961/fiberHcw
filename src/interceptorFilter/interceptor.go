package interceptorFilter

import (
	"github.com/gofiber/fiber/v2"
)

type interceptor struct {
	name          string
	preHandler    fiber.Handler
	order         int
	addUrlPattern []string
}

func CreateInterceptor(name string, perHandler fiber.Handler, order int, addUrlPattern ...string) interceptor {
	return interceptor{name: name, preHandler: perHandler, order: order, addUrlPattern: addUrlPattern}
}

func GetInterceptorPreHandler(interceptor interceptor) fiber.Handler {
	return interceptor.preHandler
}

func GetInterceptorUrlRegexPattern(interceptor interceptor) []string {
	return interceptor.addUrlPattern
}

func GetName(interceptor interceptor) string {
	return interceptor.name
}
