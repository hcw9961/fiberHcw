package interceptorFilter

import "log"

type interceptorCollector []interceptor

var interceptors interceptorCollector

func AddInterceptor(interceptorsAdd ...interceptor) {
	log.Print("[fiberHcw] [interceptorFilter]: add interceptorFilter: ", interceptorsAdd)
	interceptors = append(interceptors, interceptorsAdd...)
}

func GetInterceptorCollector() interceptorCollector {
	return interceptors
}

func (is interceptorCollector) Len() int {
	return len(is)
}

func (is interceptorCollector) Less(i, j int) bool {
	return is[j].order < is[i].order
}

func (is interceptorCollector) Swap(i, j int) {
	is[i], is[j] = is[j], is[i]
}
