package request

import (
	"github.com/gofiber/fiber/v2"
)

type Mapping struct {
	Path    string
	Handler fiber.Handler
	Method  string
}
